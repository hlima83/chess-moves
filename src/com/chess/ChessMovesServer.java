package com.chess;

import java.util.Random;
import java.util.concurrent.CompletableFuture;

public class ChessMovesServer {

    public synchronized CompletableFuture<Boolean> sendMove(String move) throws InterruptedException {
        //simulate server processing
        Thread.sleep(2000);
        boolean validMove = validateMove(move);
        if (validMove)
            System.out.println("Server::Move completed: " + move);
        else
            System.out.println("Server::Invalid move: " + move);

        return CompletableFuture.completedFuture(validMove);
    }

    //TODO: randomize
    private boolean validateMove(String move) {
        return (new Random().nextInt(2) + move.hashCode()) % 2 == 0;
    }
}
