package com.chess;

public interface GameListener {
    void onMoveMade();
    void onMoveFailed();
}
