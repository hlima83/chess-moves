package com.chess;

import java.util.concurrent.CompletableFuture;

public class ChessMovesDemo {

    private static ChessMovesServer server = new ChessMovesServer();
    private static Boolean validMove;
    private static final Object lock = new Object();

    class SenderThread extends Thread {

        public void run() {
            try {
                System.out.println(Thread.currentThread().getName() + " started!");
                CompletableFuture<Boolean> moveValidation = null;
                moveValidation = server.sendMove("KA1B1");
                moveValidation.thenAccept(val -> {
                    System.out.println(Thread.currentThread().getName() + "::Move completed with success: " + val);
                    synchronized (lock) {
                        validMove = val;
                        lock.notifyAll();
                    }
                });
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    class ListenerThread extends Thread implements GameListener {

        @Override
        public void onMoveMade() {
            System.out.println(Thread.currentThread().getName() + "::performing action after successful move");
        }

        @Override
        public void onMoveFailed() {
            System.out.println(Thread.currentThread().getName() + "::performing action after failed move");
        }

        public void run() {
            try {
                System.out.println(Thread.currentThread().getName() + " started!");
                synchronized (lock) {
                    lock.wait();
                    if (validMove)
                        onMoveMade();
                    else
                        onMoveFailed();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        ChessMovesDemo chessMovesDemo = new ChessMovesDemo();
        Thread t1 = chessMovesDemo.new SenderThread();
        Thread t2 = chessMovesDemo.new ListenerThread();
        t1.start();
        t2.start();
    }
}
